﻿using Mongo.Migration.Migrations;
using MongoDB.Bson;
using System;

namespace MythicTable.Campaign.Data.Migrations.Campaign
{
    public class M001_CampaignCharacterContainer : Migration<CampaignCharacterContainer>
    {
        public M001_CampaignCharacterContainer()
            : base("0.0.1")
        {
        }

        public override void Up(BsonDocument document)
        {
            Console.WriteLine(document.ToString());
        }

        public override void Down(BsonDocument document)
        {
            Console.WriteLine(document.ToString());
        }
    }
}
