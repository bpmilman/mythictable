﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MythicTable.GameSession
{
    public interface IGameState
    {
        Task<bool> ApplyDelta(SessionOpDelta delta);

        Task<IEnumerable<dynamic>> GetGameState();

        Task<bool> Undo(string undoPlaceholder);

        Task<bool> Redo(string redoPlaceholder);
    }
}
