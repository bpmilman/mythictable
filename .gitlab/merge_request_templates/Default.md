## Description

<!-- Briefly describe what this MR is about. -->

## Related issues

<!-- Link related issues below. Insert the issue link or reference after the word "Closes" if merging this should automatically close it. -->
* Closes #

## Author's checklist

- [ ] Follow the [Documentation Guidelines](https://gitlab.com/mythicteam/mythictable/-/wikis/documentation-guidelines) and [Coding Guidelines](https://gitlab.com/mythicteam/mythictable/-/wikis/coding-guidelines).
- [ ] Any data migration scripts have been included.

## Review checklist

All reviewers can help ensure accuracy, clarity, completeness, and adherence to the Guidelines above. Below is a list of actions or areas to consider:

- [ ] Checkout branch and run it locally
- [ ] Read the linked issue
- [ ] Pay special attention to code found here ...
- [ ] Pay special attention to code found here ...
- [ ] Confirmed logs
- [ ] Confirmed analytics
